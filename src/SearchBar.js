import React from "react";
import "./SearchBar.css";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = { searchTerm: "" };
  }
  onSearch = event => {
    console.log("changed!");
    this.setState({ searchTerm: event.target.value.toLowerCase() });
  };
  onSubmit = event => {
    event.preventDefault();
    this.props.onSearch(this.state.searchTerm);
    console.log("Ik heb op enter gedrukt");
  };
  render() {
    return (
      <div>
        <form onSubmit={this.onSubmit}>
          <input
            className="searchbar"
            type="text"
            placeholder="Zoek hier"
            value={this.state.searchTerm}
            onChange={this.onSearch}
          />
        </form>
      </div>
    );
  }
}

export default SearchBar;
