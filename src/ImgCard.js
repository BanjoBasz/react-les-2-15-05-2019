import React from "react";

const ImgCard = props => {
  return (
    <div>
      <h1>
        Name: {props.name} Number: {props.number}
      </h1>
      <img src={props.imgSrc} />
    </div>
  );
};

export default ImgCard;
