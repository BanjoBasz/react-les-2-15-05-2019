import React from "react";
import SearchBar from "./SearchBar";
import ImgCard from "./ImgCard";
import axios from "axios";
import "./App.css";

class App extends React.Component {
  state = { img: "", name: "", number: "" };
  onSubmit = searchTerm => {
    axios.get(`https://pokeapi.co/api/v2/pokemon/` + searchTerm).then(res => {
      this.setState({
        img: res.data.sprites.front_default,
        name: res.data.name,
        number: res.data.id
      });
    });
  };

  render() {
    return (
      <div className="container">
        <SearchBar onSearch={this.onSubmit} />
        <ImgCard
          name={this.state.name}
          number={this.state.number}
          imgSrc={this.state.img}
        />
      </div>
    );
  }
}

export default App;
